


Using the NewsKit framework in your project:

1) Add NewsKit.framework to your xcode project. 
2) #import <NewsKit/NewsKit.h> in your source file 





1) Using NCAdKit

Use the  NCDFPAdView to load ads using NCAdKit.

Initialising NCDFPAdView
	Example:		
	adUrl = @"..."; //URL for ads
			NCDFPAdView *adView = [NCDFPAdView new];
			adView.adViewDelegate = self;
			adView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
			
			[[NCAdKit ads] adShellForAdURL:[NSURL URLWithString:adUrl] completion:^(NCAdShell *shell, NSError *error) {
				if(!error) {
					adView.adShell = shell;
				}
			}];
			
			_adShellView = adView;
			_adShellView.alpha = 0;
			_adShellView.clipsToBounds = YES;
			[self addSubview:_adShellView];
			
			
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(adViewLoaded:)
														 name:NCAdViewLoadedNotification
													   object:_adShellView];


Implement delegate methods of NCDFPAdViewDelegate to do custom actions on ad view click events

- (void)handleClick:(NSURL *)url {
	// Any custom actions on tapping anywhere in the ad view.
    
}



2) Using NewsKit

Login to News+ (using Person API)

[[NewsKit sharedInstance] getUserWithEmail:username password:password completion:^(NCUser *user, NSError *error) {
		BOOL success = user && !error;
		completion(success, error);
	}];



3) Using NCContent

Retrieve content from Content API (CAPI)

   [NCContent retrieveContentByVideoOOyalaEmpCode:code resultHandler:^(NCContentItem *item, NSError *error) {
        if (error) {
            //error handling
        } else {
			// do your thing
        }
   }];




		
