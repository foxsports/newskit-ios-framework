//
//  NCContentItem.h
//  NewsKit
//
//  Created by Childs, Gordon on 9/5/13.
//  Copyright (c) 2013 News Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, NCContentType){
	NCContentTypeNewsStory,
	NCContentTypeImage,
	NCContentTypeVideo,
	NCContentTypeImageGallery,
	NCContentTypeCollection
};

@class NCContentImage;

@interface NCContentItem : NSObject

@property (nonatomic, readonly) NCContentType contentType;

@property (nonatomic, readonly) NSArray *authors;	// array of NSStrings
@property (nonatomic, readonly) NSDate *dateUpdated;
@property (nonatomic, readonly) NSString *contentDescription;
// TODO: domains
@property (nonatomic, readonly) NSString *contentID;
@property (nonatomic, readonly) NSArray *keywords;	// array of NSStrings

@property (nonatomic, readonly) NSURL *link;
@property (nonatomic, readonly) NSString *originalSource;

// TODO: paidStatus?

/// NSArray of NCContent, when NCNewsKitContentIncludeRelated = YES
@property (nonatomic, readonly) NSArray *relatedContent;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *subtitle;

@property (nonatomic, readonly) NCContentImage *thumbnailImage;

@end

@interface NCContentNewsStory : NCContentItem
@property (nonatomic, readonly) NSString *kicker;

/// requires NCNewsKitContentIncludeBodies = YES
@property (nonatomic, readonly) NSString *body;
@end

@interface NCContentImage : NCContentItem
@property (nonatomic, readonly) NSString *format;	// or enum?
@property (nonatomic, readonly) NSUInteger width;
@property (nonatomic, readonly) NSUInteger height;
// TODO: image type
@property (nonatomic, readonly) NSString *imageName;

@end

// TODO: NCContentImageGallery
@interface NCContentImageGallery : NCContentItem
@end

// TODO: NCContentVideo
@interface NCContentVideo : NCContentItem
@end

// TODO: NCContentCollection
@interface NCContentCollection : NCContentItem
@end

