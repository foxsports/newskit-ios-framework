//
//  NCAdKit.h
//  adshell
//
//  Created by Childs, Gordon on 11/27/13.
//  Copyright (c) 2013 News. All rights reserved.
//

#import <Foundation/Foundation.h>

// exposing NCDFPAdView so user can decide on what to do with expired pixel
//#import "NCAdShell.h"
#import <NewsKit/NCDFPAdView.h>

@interface NCAdKit : NSObject

+ (instancetype)ads;

- (void)adShellForAdURL:(NSURL *)adURL completion:(void (^)(NCAdShell *shell, NSError *error))completion;

- (NSURL *) getOfflineCreative;
@end

/// called on a status=200 pixel response
/// object: NCAdShell
extern NSString * const NCAdShellPixelCalledNotification;

/// AdView can no longer generate views, this would be a good time to fetch a new ad shell
/// object: NCDFPAdView
extern NSString * const NCAdViewBecameInvalidNotification;

/// 
/// object: NCDFPAdView
extern NSString * const NCAdViewLoadedNotification;
