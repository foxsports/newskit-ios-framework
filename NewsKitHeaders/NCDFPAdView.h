//
//  NCDFPAdView.h
//  adshell
//
//  Created by Childs, Gordon on 12/12/13.
//  Copyright (c) 2013 News. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NewsKit/NCAdShell.h>

@protocol NCDFPAdViewDelegate <NSObject>

- (void) handleClick:(NSURL *)url;

@end
@interface NCDFPAdView : UIView

@property (nonatomic) NCAdShell *adShell;
@property (nonatomic, assign) id <NCDFPAdViewDelegate> adViewDelegate;

@end
