//
//  NCUser.h
//  NewsKit
//
//  Created by Childs, Gordon on 8/12/13.
//  Copyright (c) 2013 News Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCUser : NSObject

/// User's first name
@property (nonatomic, readonly) NSString *firstName;
/// User's surname
@property (nonatomic, readonly) NSString *lastName;

/// Customer ID
@property (nonatomic, readonly) NSString *customerID;

/// entitlements. WIP, don't use.
@property (nonatomic, readonly) NSDictionary *entitlements;

@property (nonatomic, readonly) BOOL fakePaywallDownUser;

/// Asynchronously log user out. Completion will be called with an error
/// on failure, otherwise nil.
- (void)logout:(void (^)(NSError *error))completion;

- (BOOL)isEntitledToContent:(NSString *)contentName;

@end
