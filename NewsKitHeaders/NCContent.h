//
//  NCContent.h
//  NewsKit
//
//  Created by Childs, Gordon on 8/27/13.
//  Copyright (c) 2013 News Corp. All rights reserved.
//

// TODO: document text query format

#import <Foundation/Foundation.h>
#import <NewsKit/NCContentItem.h>

@interface NCContent : NSObject

// return YES to continue with results, nil result means end of results
// with possible error.
+ (void)search:(NSDictionary *)args resultHandler:(BOOL (^)(NCContentItem *item, NSError *error))handler;

/// retrieve single content item
+ (void)retrieveContentByID:(NSString *)contentID resultHandler:(void (^)(NCContentItem *item, NSError *error))handler;

@end

// keys marked "multiple" can take either one value or an NSArray of values, e.g.
// @{ NCNewsKitContentKeywords : @"election" } or
// @{ NCNewsKitContentKeywords : @[ "election", "tennis" ] }

extern NSString * const NCNewsKitContentTypes;		// multiple of the following
	extern NSString * const NCNewsKitContentTypeNewsStory;
	extern NSString * const NCNewsKitContentTypeImage;
	extern NSString * const NCNewsKitContentTypeImageGallery;
	extern NSString * const NCNewsKitContentTypeVideo;
	extern NSString * const NCNewsKitContentTypeCollection;

extern NSString * const NCNewsKitContentCategories;	// NSString (multiple)
extern NSString * const NCNewsKitContentKeywords;	// NSString (multiple)
extern NSString * const NCNewsKitContentAuthors;	// NSString (multiple)
extern NSString * const NCNewsKitContentDomains;	// NSString (multiple)

extern NSString * const NCNewsKitContentOrigins;	// NSString (multiple) of the following
	extern NSString * const NCNewsKitContentOriginFatwire;
	extern NSString * const NCNewsKitContentOriginMethode;

extern NSString * const NCNewsKitContentOriginIDs;	// NSString (multiple)

extern NSString * const NCNewsKitContentTitle;		// NSString
extern NSString * const NCNewsKitContentSubtitle;	// NSString
extern NSString * const NCNewsKitContentDescription;	// NSString
extern NSString * const NCNewsKitContentTextQuery;	// NSString. Format???

extern NSString * const NCNewsKitContentCutoffDate;		// NSDate

extern NSString * const NCNewsKitContentIncludeRelated;	// NSNumber (@YES, @NO)
extern NSString * const NCNewsKitContentIncludeBodies;	// NSNumber (@YES, @NO)

