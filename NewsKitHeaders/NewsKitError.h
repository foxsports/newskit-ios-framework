//
//  NewsKitError.h
//  NewsKit
//
//  Created by Childs, Gordon on 8/15/13.
//  Copyright (c) 2013 News Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

// SDK originated error domain. codes to be defined below
// e.g. not authenticated (API doesn't seem interested in this!)
// TnC's not agreed to
extern NSString * const NCNewsKitSDKErrorDomain;

// NewsKit API originated errors. codes come from API
extern NSString * const NCNewsKitAPIErrorDomain;

// NewsKit general non-200 http response error
extern NSString * const NCNewsKitHTTPErrorDomain;

enum {
	NCSDKErrorUserCanceledLogin = -100,
	NCSDKErrorBadAuthenticationResponse,
	NCSDKErrorBadAdShell,
};

@interface NewsKitError : NSObject

+ (NSError *)newsKitSDKErrorWithCode:(NSInteger)code andDescription:(NSString *)description;

+ (NSError *)newsKitAPIErrorWithCode:(NSInteger)code andDescription:(NSString *)description;
@end
