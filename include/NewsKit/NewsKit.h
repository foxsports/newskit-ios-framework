//
//  NewsKit.h
//  NewsKit
//
//  Created by Childs, Gordon on 8/6/13.
//  Copyright (c) 2013 News Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NewsKit/NewsKitError.h>
#import <NewsKit/NCUser.h>
#import <NewsKit/NCContent.h>
#import <NewsKit/NCAdKit.h>

@interface NewsKit : NSObject

/*!	Returns the NewsKit singleton
	
	@return the singleton!
*/
+ (instancetype)sharedInstance;

/*!	Asynchronously fetch app config from Config API.
	@discussion
		 Bootstraps all other APIs, so call this early on.
		 For success, failure and config change notifications,
		 observe
	 
		 <ul>
		 <li>NCNewsKitConfigLoadingFailedNotification</li>
		 <li>NCNewsKitConfigLoadedNotification</li>
		 <li>NCNewsKitConfigChangedNotification</li>
		 </ul>
 
	@param appBundleId ID as configured in config API. May not be an actual bundle ID.
	@param appVersion current version of your app
	@param developerAPIKeys individual API keys for config, person, etc. keyed with NCNewsKitAPIKeys*
 */

- (void)loadWithAppBundleID:(NSString *)appBundleID appVersion:(NSString *)appVersion developerAPIKeys:(NSDictionary *)developerAPIKeys;

/*!	Asynchronously fetches the News user, performing login or token refresh as necessary
	@discussion
		Avoid caching NCUser, instead call this from time to time to allow
		reauth and entitlement refreshing.
	@param completion completion called with error, nil for success
*/

/// Gets user, may present a News Plus login
/// Avoid caching NCUser, call this from time to time to allow re-auth and refresh entitlements.
- (void)getUser:(void (^)(NCUser *user, NSError *error))completion;

/// Similar to getUser: but will not present login
- (void)getUserWithEmail:(NSString *)email password:(NSString *)password completion:(void (^)(NCUser *user, NSError *error))completion;

- (NCUser *)getCachedUser;

// Forget the cached user. Used to make sure a user who is not entitled to something gets completely logged out.
- (void)forgetCachedUser;

/// YES when app config has been loaded
@property (nonatomic, readonly) BOOL isReady;
/// The date that app config was last fetched from config API
@property (nonatomic, readonly) NSDate *lastUpdate;

/// The app specific config stored in the config API. Non-nil when isReady=YES,
@property (nonatomic, readonly) NSDictionary *config;
/// non nil when isReady=YES, the appVersion of the above config.
/// Due to config caching and app updates may not match your current version.
@property (nonatomic, readonly) NSString *appVersion;

/// Currently logged in user, may be nil. Useful for updating UI,
/// for other users prefer [getUser:]
@property (nonatomic, readonly) NCUser *loggedInUser;

@end

/// Posted when an error occurs during config loading. object is NSError
extern NSString * const NCNewsKitConfigLoadingFailedNotification; // NSError

/// Posted when config is loaded. Object is an NSNumber BOOL indicating whether
/// loaded config was cached locally or not.
extern NSString * const NCNewsKitConfigLoadedNotification; // NSNumber BOOL cached
extern NSString * const NCNewsKitConfigChangedNotification;

/// dictionary keys for various APIs as passed to loadWithAppBundleID:appVersion:developerAPIKeys:
extern NSString * const NCNewsKitAPIKeysConfigKey;
extern NSString * const NCNewsKitAPIKeysPersonKey;
extern NSString * const NCNewsKitAPIKeysContentKey;

/// Posted when logged in user changes, via logging in, logging out, or when entitlements change
/// object is NCUser
extern NSString * const NCNewsKitAuthenticationStateChangedNotification; // NCUser

