//
//  NCAdShell.h
//  adshell
//
//  Created by Childs, Gordon on 12/18/13.
//  Copyright (c) 2013 News. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCAdShell : NSObject

@property (nonatomic) NSString *pixel;

@property (nonatomic) NSURL *creativeURL;
@property (nonatomic) NSString *title;

@property (nonatomic) NSDictionary *variables;

@property (nonatomic, readonly) BOOL pixelValid;

- (void)callPixel;

@end
